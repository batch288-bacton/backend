// console.log("Hello World");	

// Functions 
	// Functions in JavaScript are lines/ blocks of codes that tell our device/application to perform a certain task when called/invoke.
	// Functions are mostly created to create complicated task to run serveral lines of code in succession.

// Function declarations
	// defines a function with the specified parameters
	// Syntax:
		/*
			function functionName(){
				codeBlock/statements;
			}
		*/

	// function keyword - used to defined a JavaScript function
	// functionName - the function name. Functions are named to be able to use later in the code for the invocation.
	// function block({}) - the statements which comprise the body of the function. This is where the code will be executed.

		// Example:
			function printName(){
				console.log("My name is John!");
			}
// Function Invocation
	// The code block and statements inside a function is not immedately executed when the function is defined. The codeblock and statements inside the function is executed when the function is invoked or called.
	// It is common to use the term "call a function" instead of "invoke a function".

			// Let's invoke/ call a function that we declare.
			printName();

// Function Declaration vs Expression
	// Function Declaration
		// A function can ve created through function declaration by using the function keyword and adding the function name.
		// Declared function/s can be hoisted.

		declaredFunction();
		// Note: Hoisting is a JavaScript behavior for certain variable and functions to run or use before their declaration.

		function declaredFunction(){
			console.log("Hello World from declaredFunction!")
		}

	// Function Expression
		// A function can also be stored in a variable.
		// A function expression is an anonymous function assigned to a variable function.

		let variableFunction = function(){
			console.log("Hello Again!");
		};

		variableFunction();

		// We can also create a function expression of a named function.

		let funcExpression = function funcName(){
			console.log("Hello from the other side!")
		}

		funcExpression();

		// You can reassisng delcared functions and function expression to a new anonymous function.

		declaredFunction = function(){
			console.log("Updated declaredFunction");
		};

		declaredFunction();

		funcExpression = function(){
			console.log("Updated funcExpression");
		};

		funcExpression();

		// However, we cannot re-assign a function expression initialized with const. 

		const constantFunc = function(){
			console.log("Initialize with const!");
		};

		constantFunc();

		/*constantFunc = function() {
			console.log("Cannot be re-assigned!");
		};

		constantFunc();*/

// Function Scoping
	// Scope is the accessibility of variables within our program.

	/*
		JavaScript Variables has 3 types of scope:
		1. Gobal scope
		2. Local/block scope
		3. Function scope
	*/

		{
			let localVar = "Armando Peres";
		}

		// Result in error. localVar, being in a block, it wil not be accessible outside the block.
		/*console.log(localVar);*/

		let globalVar = "Mr. Worlwide";

		{
			console.log(globalVar);
		}

	// Function Scope
		// JavaScript has function scope: Each function creates a new scope.
		// Variables defined inside a function are not accessib;e(visible) from outside the function.

		function showNames(){
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionConst);
			console.log(functionLet);
		}

		showNames();

	// Nested functions
		// You can create another function inside a function. This is called a nested function.

		function myNewFunction(){
			let name = "Jane";

			// nested function
			function nestedFunction(){
				console.log(name);
			}

			nestedFunction();
		}

		myNewFunction();

// Return Statement
		/*
			The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
		*/

		function returnFullName(){
			let fullName = "Jeffry Smith Bezos";

			return ("Chris");

		}

		returnFullName();

		console.log(returnFullName());

		let fullName = returnFullName();

		console.log(fullName);


// Function Naming Conventions
		// Function names should be definitive of the taks it will perform. It usually contains a verb.

		function getCourses(){
			let courses = ["Science 101", "Match 101", "English 101"];

			return courses;
		}


		// Avoid generic names to avoid confusion within our code.

		function get(){
			let name = "Jamie";

			return name;
		}

		// Avoid pointless and inappropriate function names.

		function foo(){

			return 25 % 5;
		}

		// Name your functions in small caps. Follow camleCase when naming variables and functions with multiple words.

		function displayCarInfo(){
			console.log("Brand: TOYOTA");
			console.log("Type: Sedan");
		}	