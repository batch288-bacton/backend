/*console.log("Hello World!");*/

/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/

	function getUserInfo(){
		let key = {
			name: "Jake Sully",
			age: 38,
			address: "Alpha Centauri A system",
			isMarried: true,
			petName: "Bob",
		}

		return key;
	}
	console.log("getUserInfo();", getUserInfo());


/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/
	function getArtistsArray(){
		let artists = ["My Chemical Romans", "Fall Out Boy", "Secondhand Serenade", "FM Static", "The Red Jumpsuite Apparatus"];

		return artists;
	}

	console.log("getArtistsArray();", getArtistsArray());


/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getSongsArray(){
		let songs = ["Welcome to the Black Parade", "Centuries", "Stay Close, Dont Go", "Tonight", "Your Guardian Angel"];

		return songs;
	}

	console.log("getSongsArray();", getSongsArray());

/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/
	function getMoviesArray(){
		let movies = ["Grey Hound", "Pope's Exorcist", "Lord Of The Rings - Trilogy", "Top Gun: Maverick", "Avatar"];

		return movies;
	}

	console.log("getMoviesArray();", getMoviesArray());



/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/
	function getPrimeNumberArray(){
		const primeNumber = [2, 3, 5, 7, 11]

		return primeNumber;
	}

	console.log("getPrimeNumberArray();", getPrimeNumberArray());




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){
 

}