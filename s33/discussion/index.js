// console.log("Hello!");

// [Section] JavaScript Synchronous vs Asynchronous
	// JavaScript is by default synchronous meaning that only one statement is executed a time.

	// This can be proven when a statemtn has an error, JavaScript will not proceed with the next statement.

/*console.log("Hello World!");

conole.log("Hello after the world");

console.log("Hello");*/

	// When cerain statements take a lot of time to process, this slows down our code.

	/*for(let index = 0; index <= 1500; index++){
		console.log(index);
	}

	console.log("Hello again!")*/

	// Asynchronous means that we can proceed to execute other statements, whole time consuming code is running in the background.

// [Section] Getting all posts

	// The Fetch API allows you to asynchronously request for aresource data.
	// so it means the fetch method that we are going to use here wil run asynchronouslly.

	// Syntax:
		// fetch('URL');
	
	// A "promise" is an object that represents the eventual completion (or failure) of an asunchrnous function and it's resulting value.
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
		/*
			fetch('URL');
			.then((response => response)); 
		*/

	// Retrive all post follow the REST API.
	// The fetch method will return a promise that resolves the Response object.
	// the "then" method captures the reponse object.
	// Use the "json" method from the Response object to convert the data retrieved into JSON format to be used in our application.
	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(json => console.log(json))

	// The "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code.

	// Creates an asynchronous function.

	async function fetchData(){

		let result = await fetch('https://jsonplaceholder.typicode.com/posts');
		console.log(result);

		let json = await result.json();
		console.log(json);
	};

	fetchData();

// [Section] Getting specific post

	// Retrieved specific post follow the rest API(/posts/:id)

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then(response => response.json())
	.then(json => console.log(json))

// [Section] Creating Post

	// Syntax:
		/*
			// options is an object that contains the method, the header and the body of the request.

			// by default if you don't add the method in the fetch request, it will be a GET method.

			fetch('URL', options)
			.then(response => {})
			.then(response => {})
		*/
	fetch('https://jsonplaceholder.typicode.com/posts', {
		// sets the method of the request obhect to post following the rest API
		method: 'POST',
		// sets the header data of the requst obhect to be sent to the backend.
		// specified that the content will be in JSON structure.
		headers: {
			'Content-Type':'application/json'
		},
		// sets the content/body data of the request object to be sent backend.
		body: JSON.stringify({
					title: 'New post',
					body: 'Hello World',
					userID: 1
				})
	})
	.then(response => response.json())
	.then(json => console.log(json));

// [Section] Update a specific Post

	// PUT Method 
	// a method of modifying resource where the client sends data that updates the entire object/documents
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
					id: 1,
					title: 'Updated Post',
					body: 'Hello Again!',
					userID: 1
				})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// PATCH Method 
	// applies a partial update to the object or document
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PATCH",
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
					title: 'Updated Post',
				})
	})
	.then(response => response.json())
	.then(json => console.log(json));

// [Section] Deleting a post

	// deleting specific post following the REST API
	fetch('https://jsonplaceholder.typicode.com/posts/1', {method: "DELETE"})
	.then(response => response.json())
	.then(json => console.log(json))
