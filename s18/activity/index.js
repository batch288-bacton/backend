/*console.log("Welcome!")*/

/*
	
	1. Create a function called addNum which will be able to add two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the addition.
	   
	   Create a function called subNum which will be able to subtract two numbers.
	    - Numbers must be provided as arguments.
	    - Return the result of subtraction.

	    Create a new variable called sum.
	     - This sum variable should be able to receive and store the result of addNum function.

	    Create a new variable called difference.
	     - This difference variable should be able to receive and store the result of subNum function.

	    Log the value of sum variable in the console.
	    Log the value of difference variable in the console.

	2. Create a function called multiplyNum which will be able to multiply two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the multiplication.

		Create a function divideNum which will be able to divide two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the division.

		Create a new variable called product.
		 - This product variable should be able to receive and store the result of multiplyNum function.

		Create a new variable called quotient.
		 - This quotient variable should be able to receive and store the result of divideNum function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.


	3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
		- a number should be provided as an argument.
		- look up the formula for calculating the area of a circle with a provided/given radius.
		- look up the use of the exponent operator.
		- return the result of the area calculation.

		Create a new variable called circleArea.
		- This variable should be able to receive and store the result of the circle area calculation.
		- Log the value of the circleArea variable in the console.

	4. Create a function called getAverage which will be able to get total average of four numbers.
		- 4 numbers should be provided as an argument.
		- look up the formula for calculating the average of numbers.
		- return the result of the average calculation.
		
		Create a new variable called averageVar.
		- This variable should be able to receive and store the result of the average calculation
		- Log the value of the averageVar variable in the console.
	

	5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
		- this function should take 2 numbers as an argument, your score and the total score.
		- First, get the percentage of your score against the total. You can look up the formula to get percentage.
		- Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
		- return the value of the variable isPassed.
		- This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
	// For Addition:
	function addNum(num1, num2){
		
		console.log("Displayed sum of " + num1 + " and " + num2);

		let addition = num1 + num2;

		return addition;
	}

	let sum = addNum(5, 15);
	console.log(sum);


	// For Subtraction:
	function subNum(num3, num4){
		
		console.log("Displayed difference of " + num3 + " and " + num4);

		let subtract = num3 - num4;

		return subtract;
	}

	let difference = subNum(20, 5);
	console.log(difference);


	// For Multiplication:
	function multiplyNum(num5, num6){
		
		console.log("The product of " + num5 + " and " + num6 + ":");

		let multiply = num5 * num6;

		return multiply;
	}

	let product = multiplyNum(50, 10);
	console.log(product);


	// For Division:
	function divideNum(num7, num8){
		
		console.log("The quotient of " + num7 + " and " + num8 + ":");

		let divide = num7 / num8;

		return divide;
	}

	let quotient = divideNum(50, 10);
	console.log(quotient);


	// For Circle Area:
	function getCircleArea(pi, radius){
		
		console.log("The result of getting the area of a circle with " + radius + " radius " + ":");

		let circleArea = 3.1416 * radius ** 2;

		return circleArea;
	}

	let circleArea = getCircleArea(3.1416, 15);
	console.log(circleArea);


	// For Average Operator:
	function getAverage(){
		console.log("The average of 20,40,60 and 80: ");
		let average = (20 + 40 + 60 + 80) / 4;

		return average;
	}

	let averageVar = getAverage();
	console.log(averageVar);


	// Check is Passed:

	function checkIfPassed(score, totalScore){
		console.log("Is " + score + "/" + totalScore +" a passing score?");

		let percentage = (score / totalScore)* 100;

		let isPassed = percentage > 75;

		return isPassed;
	}
	let isPassingScore = checkIfPassed(38, 50);
	console.log(isPassingScore);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}