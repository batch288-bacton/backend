// To check whether the script file is properly linked to your html file.
/*console.log("hello")*/

// [Section] Arithmetic Operators
	let x = 1397;
	let y = 7831;

	// Addition Operator (+)
	let sum	= x + y;

	console.log("Result of addition operator :" + sum);

	// Subtraction Operator (-)
	let difference = y - x;

	console.log("Result of subtraction operator:" +difference);

	// Multiplication Operator (*)
	let product = x * y;

	console.log("Result of multiplication operator:" + product);

	// Division Operator (/)
	let quotient = y / x;

	console.log("Result of division operator:" + quotient);

	// Modulo Operator (%)
	let remainder = y % x;

	console.log("Result of modulo operator: " + remainder);

// [Section] Assignment Operators
	// Assignment Operator (=)
	// The assignment operator assigns/reassign the result to the variable

	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// The addition assignment operators adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber += 2;

	console.log(assignmentNumber);

	assignmentNumber += 3;

	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Substraction/Multiplication/Division Assignment Operator (-=. *=, /=)

	// Subtraction Assignment Operator

	assignmentNumber -= 2;

	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	// Multiplication Assignment Operator

	assignmentNumber *= 3;

	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	// Division Assignment Operator

	assignmentNumber /= 11;

	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses
	// MDAS - Multiplication or Division First, then Addition or Subtraction. From left to right

	let mdas = 1 + 2 - 3 * 4 / 5;
	/* How it works:
		1. 3* 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	console.log(mdas.toFixed(1));


	let pemdas = 1 + (2-3) * (4/5);
	/* How it works:
		1. 4/5 = 0.8
		2. 2-3 = -1
			1 + (- 1) * (0.8)
		3. -1 * 0.8 = -0.8
		4. 1 - 0.8
		0.2
	*/

	console.log(pemdas.toFixed(1));

//  [Section] Increment and Decrement
	//  Operators that add or subtract values by 1 and reassigns the value of the variable where the increment and decrement applied to.

	let z = 1;
	// Pre-increment example:

		let increment = ++z;

	console.log("Result of pre-increment: " + increment);

	console.log("Result of pre-increment: " + z);

	// Post-increment example:

	increment = z++;
	console.log("The result of post-increment: " + increment);

	console.log("The result of post-increment: " + z);

	// Decrement example:

	x = 5;

	// Pre-decrement example:

	let decrement = --x;

	console.log("Result of pre-decrement: " + decrement);

	console.log("Result of pre-decrement:" + x);

	// Post-decrement example:

	decrement = x--;

	console.log("Result of post-decrement: " + decrement);

	console.log("Result of post-decrement: " + x);

// [Section] Type Coercion
	/*
		-Type Coercion is the automatic or implicit converion of values from one data type to another.
		-This happens when operations are performed on different data types that would normally possible and yield irregular reuslts.
	*/

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	// If you are going to add string and number, it will concatenate its value
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	// The boolean "true" is also associated with the value of 1
	let numbE = true + 1;
	console.log(numbE);

	// The boolean "false" is also associated with the value of 0
	let numbF = false + 1;
	console.log(numbF);

// [Section] Comparison Operators

	let juan = "juan";

	// Equality Operator (==)
	/*
		- Checks whether the operands are equal/have the same content/value.
		- Attempts to CONVERT and COMPARE operands of different data types.
		- Returns a Boolean value.
	*/

	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true

	// compare two strings that are the same
	console.log('juan' == 'juan') //true
	console.log('JUAN' == 'juan') //false (case-sensitive)

	console.log(juan == "juan") //true

	// Inequality Operator (!=)
	/*
		- Checks whether the operands are not equal/have different content/values.
		- Attempts to CONVERT and COMPARE operands of different data types.
		- Returns Boolean Value.
	*/

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(0 != false); //false
	console.log('JUAN' != 'juan'); //true
	console.log(juan != "juan"); //false

	// Strict Equality Operator (===)
	/*
		- Checks whether the operands are equal/have the same content/values.
		- Also COMPARE the data types of two values.
	*/
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(0 === false); //false
	console.log('JUAN' === 'juan'); //false
	console.log(juan === "juan"); //true

	// Strictly Inequality Operator (!==)
	/*
		- Checks whether the operands are not equal/have different content
		- Also COMPARES the data types of 2 values.
	*/
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false); //true
	console.log('JUAN' !== 'juan'); //true
	console.log(juan !== "juan"); //false

// [Section] Relational Operator 
// Checks whether one value is greater or less than other values.

	let a = 50;
	let b = 65;

	// GT or Greater Than Operator (>)
	let isGreaterThan = a > b; //false
	console.log(isGreaterThan);

	// LT or Less Than Operator (<)
	let isLessThan = a < b; //true
	console.log(isLessThan);

	// GTE or Greater Than or Equal Operator (>=)
	let isGTorEqual = a >= b; //false
	console.log(isGTorEqual);

	// LTE or Less Than or Equal Operator (<=)
	let isLTorEqual = a <= b; //true
	console.log(isLTorEqual);

	let numStr = "30";
	console.log(a > numStr); //true
	console.log(b <= numStr); //false

	let strNum = "twenty"; 
	console.log(b >= strNum); //false
	// Since the string is not numeric, the string was converted to a number and it resulted to NaN (Not a Number)

// [Section] Logical Operators
	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&& - Double Ampersand)
	// Returns "true" if all operands are true.
	let allRequirementsMet = isLegalAge && isRegistered; 
	console.log("Result of logical AND operator: " + allRequirementsMet); //false

	// Logical OR Operator (|| - Double Pipe)
	// Returns "true" if one of the operands are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet); //true

	// Logical NOT Operator (! - Excalamation Point)
	// Returns the opposite value.
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " +someRequirementsNotMet); //true

	