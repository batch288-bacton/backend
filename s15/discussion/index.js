// Single Line Comment
/*
	Multi Line Comment
	This is a comment
*/

// [Section] Syntax, Statements and Comments
// Statements in programming, these are the instructions that we tell the computer to perform.
// JavaScript Statement usually it ends with semicolon(;)
// Semicolons are not required in JS, but we will use it to help us prepare for the other stric languages live Java.
// A syntax in programming, it is the set of rules that describes how statements must be constructed.
// All lines/blocks of code should be weitten in a specific or else the statement will not run.

// [Section] Variables 
// It is used to contain/store data.
// Any information that is used by an applicaiton is stored in what we call memory.
// When we create variables, certain portion of a device memory is given a "name" that we call "variables".

// Declaring Variables
// Declaring Variables - tells our devices that a variable name is created and ready to store data
	// Syntax:
	// let/const variableName

let myVariable;

// by default if you declare a variable and did not initialize its value it will become "undefined"

// console.log() is useful for printing values of a variable or certain results of code into the Google Chromes Browser's console
console.log(myVariable);

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choice and use the assignment operator(=) to assign a value.
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. For constant variables, use 'const' keyword.
		4. Variable names, it should be indicative(descriptive) of the value being stored to avoid condusion.
*/

// Declaring and Initializing variables
// Initializing variables - the instance when a variable it's initial or starting value.
	// Syntax
		// let/const variableName = value;

// example:
let productName = 'desktop computer';

console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not change.
// In this example, the interest rate for a loan or savings account or a mortgage must not change due to real world concerns.

const interest = 3.539;

// Reassigning Variable values
// Reassigning a variable, it means chanign it's initial or previous into another value.
	// Syntax
	// variableName = newValue

productName = 'Laptop';
console.log(productName);

// The value of a variable declared using the const keyword can't be reassigned.

// interest = 4.489;
// console.log(interest);

// Reassigning variables vs Initializing Variables
// Declares a variable

let supplier;
// Initializing
supplier = "John Smith Trading";
// Reassigning
supplier = "Uy's Trading";

// Declaring and initializing a variable
let consumer = "Chris";

// Reassigning
consumer = "Topher"

// Can you declare a const variable without Initialization.
	// No. An error will occur

/*const driver;

driver = "Warlon Jay"*/

// var vd let/const keyword
	// var - is also used in declaring variables. But var is an EcmaScript 1 version (1997)
	// let/const keyword was introduced as a new feature in ES6(2015)

// What makes let/const different from var?
	// There are issues associated with variables declared/created using var, regarding hoisting
	// Hoisting is Javascript default behavior of moving declaration to the top.
	// In terms if variable and constats, keyword var is hoisted and let and const does not allow hoisting.

	// Example of Hoisted:

	a = 5;

	console.log(a);

	/*b = 6;
	console.log(b);
	let b;*/

// let/const local/global scope 
	// Scope essentially means where these vafriables are available or accessible for use 

	// let and const are block scoped
	// A block is a chunk of code bounded by {}. A block lives in a curly braces

let outerVariable = "hello";

let globalVariable;

	{
		let innerVariable = 'hello again';

		console.log(innerVariable);
		console.log(outerVariable);

		globalVariable = innerVariable;
	}

	// console.log(innerVariable);

console.log(globalVariable);

// Multiple variable declarations and Initialization.
// Multiple variables may be declared in one statement

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// Multiple variables to be consoled in one line.
console.log(productCode, productBrand);

// Using a variable with a reserved keyword.
// reserved keywords cannot be used as a variable name as it has function in JavaScript.
// const let = "Hi Im let keyword";

// console.log(let)

// [Section]Data Types

// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text.
// Strings in JavaScript can be written using either a single('') or double ("") quote

	let country = 'Philippines';
	let province = "Metro Manila";
	// Concatenation of Strings in JavaScript
		// Multiple string values can be combined to create a single string using the "+" symbol

	let fullAddress = province +', '+ country;
	console.log(fullAddress);

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	/*
		- The excape characters (\) in strings in combination with other characters can produce different effects/results.
		- "\n" this creates a next line in between text.
	
	*/

	let mailAddress = 'Metro Manila\n\nPhilippine';
	console.log(mailAddress);

	let message = "John's employees went home early.";
	console.log(message);

	message = 'John\'s employees went home early.'
	console.log(message);

	// Numbers
	// Intgers/Whole Numbers
	let headcount = 26;
	console.log(headcount);

	// Decimal Number/Fractions
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// Combining text and strings
	console.log("John's grade last quarter is " +grade);

	// Boolean
	// Boolean values are normaly used to create values relating to the state of certain things.
	let isMarried = false;
	let isGoodConduct = true;
	console.log("isMarried: " +isMarried);
	console.log("isGoodConduct: " +isGoodConduct);

	// Arrays
	// Arrays are special kind of data that's used to store multiple related values.
	// In JavaScript, Arrays can store different data types but is normally used to store similar data types.

	// similar data types
	// Syntax
		// let/const arrayName = [elementA, elementB, elementC, ...]

	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	// different data types
	let details = ["John", "Smith", 32, true];
	// not recommended in using array
	console.log(details);

	// Objects
	// Objects are another special kind of data type that's use to mimic real world objects/items.
	// 

	/*
		Syntax:
		let/const objectName = {
			propertyA: valueA,
			propertyB: valueB,
		}
	
	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}

	console.log(person);

	// type of operator, is used to determine the type of data or value. It outputs string.
	console.log(typeof mailAddress);
	console.log(typeof headcount);
	console.log(typeof isMarried);

	console.log(typeof grades);

	// Note: Array is a special type of objects with methods and function to manipulate.

	// Constant Objects and Arrays
	/*
	The keyword const is a little musleading
	It does not define a constant value. It defines a constant reference to a value:

	Because of this you can not:
	Reassign a constant value.
	Reassign a constant array.
	Reassign a constant object.

	But you can:

	Change the elements of a constant array.
	Change the properties of constant object.*/

	const anime = ["One piece", "One Punch Man", "Attach on Titan"];

	/*anime = ["One piece", "One Punch Man", "Kimetsu no yaiba"];
	console.log(anime);*/

	anime[2] = "Kimetsu no yaiba";
	console.log(anime);

	// Null
	// It is used to intentionally express the absence of a value in a variable.

	let spouse = null;

	spouse = "Maria";

	// Undefined
	// Represents the state of a variable that has been declared but without an assigned value.

	let fullName; 

	fullName = "Maria";