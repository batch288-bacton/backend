const Users = require("../models/Users.js");

const bcrypt =require("bcrypt");

//Require the auth.js
const auth = require('../auth.js');
// Require the courses collection
const Courses = require("../models/Courses.js")

// Controllers

//Create a controller for the signup
//registerUser
/* Business Logic/ Flow */
 	// 1. First, we have to validate whether the user is existing or not. We can do that by valiadting whether the email exist on our databases or not.
	//2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	//3. otherwise, we will sign up or add the user in our database.
module.exports.registerUser = (request, response) => {
	
	//find method: it will return the an array of object that fits the given criteria

	Users.findOne({email : request.body.email})
	.then(result => {

		// we need add if statement to verify whether the email exist already in our database
		if(result){

			return response.send(false)
		}else{

			//Create a new Object instantiated using the Users Model.
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				//hashSync method : it hash/encrypt our password
				//the second argument salt rounds
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo 
			})


			// Save the user
			//Error handling
			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false));
		}
	})
	.catch(error => response.send(false));

}

//new controller for the authentication of the user
module.exports.loginUser = (request, response) => {

	Users.findOne({email : request.body.email})
	.then(result => {

		if(!result){
			return response.send(false)
		}else{
			//the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending the result of the comparison.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send(
					{auth: auth.createAccessToken(result)}
				)
			}else{
				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false));
}

module.exports.getProfile = (request, response ) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return Users.findById(request.body._id).then(result => {

					result.password = "Confidential";

					return response.send(request);

				}).catch(error => response.send(error));
	}else{
		return response.send(`You are not an admin, you don't have access to this route.`);
	}
}

// Controller for the enroll course
module.exports.enrollCourse = (request, response) => {

	const courseId = request.body.id;

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false)
	}else{

		// Push to user document
		let isUserUpdated = Users.findOne({_id: userData.id})
		.then(result => {

			result.enrollments.push({
				courseId: courseId

			})
			result.save()
			.then(save => true)
			.catch(error => false)

		})
		.catch(error => false)


		// Push to course document

		let isCourseUpdated = Courses.findOne({_id: courseId})
		.then(result => {

			result.enrollees.push({userId: userData.id})

			result.save()
			.then(saved => true)
			.catch(error => false)

		}).catch(error => false)

		// If condition to check whether we updated the users document and courses documents

		if(isUserUpdated && isCourseUpdated){
			return response.send(true)
		}else{
			return response.send(false)
		}

	}

}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data))
}