const express = require("express");

const usersControllers = require('../controllers/usersControllers.js');

const auth = require("../auth.js")

const router = express.Router();

//Routes

//route for registration
router.post("/register", usersControllers.registerUser);

//login
router.post("/login", usersControllers.loginUser);

//getProfile
router.get("/details", auth.verify, usersControllers.getProfile);

router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

// route for course enrollment
router.post("/enroll", auth.verify, usersControllers.enrollCourse);





//tokenVerification
// router.get("/verify", auth.verify);

module.exports = router;