const express = require("express");

const coursesControllers = require("..//controllers/coursesControllers.js");

const auth = require("../auth.js")

const router = express.Router();


// Routes without params here:


// This route is responsible for adding course in our db.
router.post("/addCourse", auth.verify, coursesControllers.addCourse);


// Route for retrieving all courses
router.get("/", auth.verify, coursesControllers.getAllCourses);


// Route for retrieving all courses.
router.get("/activeCourses", coursesControllers.getActiveCourses);


// route for inactive course
router.get("/inActiveCourses", auth.verify, coursesControllers.getInactiveCourses);


// Route for getting the specific courses information.
router.get("/:courseId", coursesControllers.getCourse);



// Routes with params here:


// route for updating course
router.put("/:courseId", auth.verify, coursesControllers.updateCourse);


// route for archive course
router.patch("/:courseId/archive", auth.verify, coursesControllers.archiveCourse);


module.exports = router;