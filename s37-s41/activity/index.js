const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const usersRoutes = require("./routes/usersRoutes.js");
const coursesRoutes = require("./routes/coursesRoutes.js");

const port = 4002;

const app = express();

// Set up MongoDB connection
// Establish the connection
mongoose.connect("mongodb+srv://admin:admin@batch288bacton.su474nf.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {useNewUrlParser:true, useUnifiedTopology: true});

// Check if connected to the database
const db = mongoose.connection;
		
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));

	db.once("open", () => console.log('We are now connected to the Cloud database!'));

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors())

// add the routing of the routes from the usersRoutes
app.use("/users", usersRoutes);

app.use("/courses", coursesRoutes);


app.listen(port, () => console.log(`The server is running at port ${port}`))