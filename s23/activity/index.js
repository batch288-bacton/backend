
// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	}
};
console.log(trainer);

// Initialize/add the given object properties and methods

trainer.talk = function(){
		console.log("Pikachu I choose you!");
}

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon

// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

function pokemon(name, level){

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level

	// Methods
	this.tackle = function(target){
		target.health = (target.healt - this.attack)
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + " health is now reduced to " + target.health);
		if(target.healt <= 0){
			this.faint(target)
		}
	}
	this.faint = function(target){
		console.log(target.name + 'fainted');
	}
}

let pikachu = new pokemon("Pikachu", 12);
let geodude = new pokemon("Geodude", 8);
let mewtwo = new pokemon("Mewto", 100);






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}