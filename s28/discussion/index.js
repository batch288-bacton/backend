// show databases - list of the db inside our cluster
// use 'dbName' - to use a specific database
// show collections - to see the list of collections inside the db.

// CRUD operations
/*
	- CRUD operation is the heart of any backend application.
	- mastering the CRUD operation is essential for any developer especially to those who want to become backend developer
*/
// [Section] Inserting Documnets (Create)
	// Insert one document
		/*
			- Since mongoDB deals with objects as it's structure for documnets we can easily create them by providing objects in our method/operation.

			Syntax: 
				db.collectionName.insertOne({
					object
				})
		*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact : {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	course: ["CSS", "JavaScript", "python"], 
	department: "none"
})

// Insert Many

/*
	syntax:
		db.collectionName.insertMany([{objectA},{objectB}]);
*/

db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	])

//[Section] Finding documents (Read operation)
	// db.collectionName.find();
	// db.collectionName.find({field:value});
// Using the find() method, it will show you the list of all the documents inside our collection
db.users.find();

// The "pretty" methods allows us to be able to view the documents returned by our terminals to be in a better format
db.users.find().pretty();

// It will return the documents that will pass the criteria fiven in the method
db.users.find({firstName:"Stephen"});

// Find using ID
db.users.find({_id: ObjectId("646c557c9d268395a42ae3c5")});

// mutiple criteria
db.users.find({lastName : "Armstrong", age: 82});

db.users.find({firstName : "Jane"});

db.users.find({"contact.phone" : "123456789"});

//[Section] Updateing Documents(Update)

db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

// updateOne method
/*
	Syntax:
	db.collectionName.updateOne({criteria, {$set: {field:value}}});
*/

db.users.updateOne(
	{firstName: "test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTMl"],
			department: "Operations",
			status: "none"
		}
	}
);


db.users.updateOne(
	{firstName : "Jane"},
	{
		$set: {
			lastName: "Edited"
		}
	}
	);

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany(
			{criteria},
			{
				$set: {
					{field:value}
				}
			}
		)
*/

db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
	)

// Replace One
	/*
		Syntax: db.collectionName.replaceOne(
		{criteria}, {
			$set: {
				object
			}
		})
	*/
db.users.insertOne({firstName: "test"});

db.users.replaceOne(
	{firstName: "test"},
	{

			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: [],
			courses: [],
			department: "Operations"
		}
	)

// [Section] Deleting Documents
// Deleting single documents
	/*
		db.collectionName.deleteOne({criteria})
	*/

db.users.deleteOne({ firstName : "Bill"});

// Deleting multiple document
	
	/*
		db.collectionName.deleteMany({criteria})
	*/

db.users.deleteMany({firstName: "Jane"});

// All the documents in our collection will be deleted.
db.users.deleteMany({});