/*console.log("Welcome!");*/

// An array in programming is simple a list of data that shares the same data type.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Now, with an array, we can simple write the code above like this:

let studentNumbers = ['202-1923', '202-1924', '202-1925', '202-1926', '202-1927'];

// [Section] Arrays 
	// Arrays are used to store multiple related values in a single variable.
	// They are declared using the square bracket([]) also known as "Array Literals".

	/*
		Syntax:
			let/const arrayName = [elementA, elementB, elementC, . . . .];
	*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Toshiba'];

// Possible use of an array but is not recommended

let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays:

let myTasks = [
		'drink html',
		'eaat javascript',
		'inhale css',
		'bake sass'
	]

console.log(myTasks);

// Creating an array with values from variable.

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

city1 = "Osaka";

let cities = [city1, city2, city3];

console.log(cities);


// [Section] Length property
	// The .length property allows us to get and set the total number of items in an array.

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also set the total number of elements in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length of an array.

console.log(myTasks);

// myTasks.length = myTasks.length - 1;

myTasks.length -= 1;

console.log(myTasks);
console.log(myTasks.length);

// To delete a specific item in an array we can employ array methods (which will be shown/discussed in the next session) or an algorithm.

// Another example using decrementation:

console.log(cities);
cities.length--;
console.log(cities);

// We can't do the same on strings
let fullName = "Jamie Noble";
console.log(fullName);

fullName.length -= 1;
console.log(fullName);
	// If you can shorten the array by setting the length property, you cannot shorten the numbers of characters using the same property.

// Since you can shorten the array by setting the length property, you can also lengthen it by adding a number  into the length property. Since we lengthen the array forcibly, there will be another item in the array, however, it will be empty or undefined.

let theBeatles = ["John", "Ringo", "Paul", "George"]
console.log(theBeatles);

theBeatles.length += 1;
console.log(theBeatles);

// [Section] Reading from Arrays
	
	/*
		- Accessing array elements is one of the more common tasks that we can do with an array.
		- We can do this by using the array indexes.
		- Each element in an array is associated with its own index/number.

		Syntax:
			arrayName[index]
	*/

console.log(grades[0]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegends);

// You can actually save/store array items in another variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also reassign array values using items indices.
console.log('Array before reassignment:');
console.log(lakersLegends);

// Reassign the value of specific element:
lakersLegends[1] = "Pau Gasol";
console.log('Array after reassignment:');
console.log(lakersLegends);

// Accessing the last element of an array.
// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

console.log(bullsLegends[bullsLegends.length-1]);

// Adding elements into the Array

let newArr = [];
console.log(newArr);

newArr[newArr.length] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);

// [Section] Looping over an array
	// You can use a for loop to iterate over all items in an array.

	// for loop
	for(let index = 0; index < bullsLegends.length; index++){
		console.log(bullsLegends[index]);
	}

	// Example:
		// We are going to create a loop that will check whether the element inside our array is divisible by 5 or not.
		// If divisible by 5 console number + "is divisible by 5"
		// If not console the number + "is not divisible by 5"
	let numArr = [5, 12, 30, 46, 40];

	let numDivBy5 = [];
	let numNotDivBy5 = [];

	for(let index = 0; index < numArr.length; index++){

		// if statement to finter whether the number is divisible by 5 or not.
		if(numArr[index] % 5 === 0){

			console.log("The " + numArr[index] + " is divisible by 5!")

			numDivBy5[numDivBy5.length] = numArr[index];
		}
		else{
			console.log("The " + numArr[index] + " is not divisible by 5!");

			numNotDivBy5[numNotDivBy5.length] = numArr[index];
		}
	}

	console.log(numDivBy5);
	console.log(numNotDivBy5);


// [Section] Multidimensional Array
	// Multidimensional arrays are usefull for storing complex data structure.
	// Though usefull in number cases, creating complex array structures is not always recommended.

let chessboard = [['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'], ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'], ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']]

console.log(chessboard);
console.log(chessboard[0][2]);
console.log(chessboard[3][4]);

// two dimensional array
console.table(chessboard);