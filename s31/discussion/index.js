// Use the "require" directive to load Node.js modules.
// A module is a software component or part of a program that contains one or more routines.
// The "http module" lets Node.js transfer data using the hyper Text Transfer Protocol.
// In that way, HTTP is a protocol that allows the fetching of resources such as HTML documents.

// Clients (browser) and servers (Node JS/Express JS Applications) communicate by exchanging individual messages.
// These messages are sent bu the clients, usually a web browser and called "request".

// The messages sent by the server as an answer are called "response"
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client.
// A port is a virtual point where network connections starts and end.
// The HTTP module has a createServer() method that accepts a function as an arguement and allows for creation of a server.
// The arguements passed in the function are request and response objects (data types) that contains methods that allows us to receive request from the client and send responses back to it.

// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server.
http.createServer(function (request, response) {

	// Use the writeHead() method to:
	// Set a status code for the response - a 200 means OK.
	// Set the content-type of the response as a plain test message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with text content 'Hellow World?!'
	response.end('Hellow World?!');

}).listen(8000);

// When the server is running, console will print the message:
console.log('Server running at localhost:8000');