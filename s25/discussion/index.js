/*console.log("Good Morning!");*/

// [Section] JSON Objects
	// JSON stands for JavaScript Objet Notation
	// JSON is also used in other programming languages hence the name JavaScript Object Notation.
	// Core JavaScript has a built in JSON object that contains methods for parsing objects and converting strings into JavaScript objects.
	// .json file, JSON file .
	// JavaScript objects are not to be confused with JSON.
	// JSON is used for serializing different data types into bytes.
	// Serialization is the process of converting data into series of bytes for easier transmission/ transfer of information/data.
	// A byte is a unit of data that is eight binary digits ( 1 , 0 ) that is used to represent a character (letter, numbers, typographic symbols.)
	/*
		Syntax: {
			"propertyA" : "valueA",
			"propertyB"	: "valueB"
		}
	*/

// [Section] JSON Arrays
/*
	"cities" : [
		{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
	]
*/

// [Section] JSON Methods
	// The JSON object contains methods for parsing and converting data into stringified JSON.

	// Converting Data into Stringified JSON
		// Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application.
		// They are commonly used in HTTP request where infromation is required to be sent and receive in a stringified version.
		// Requests are an important type of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database.

	let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}]

	console.log(batchesArr);
	console.log(typeof batchesArr);

	// The stringify method is used to convert JavaScript Objects into a string.
	console.log('Result from stringify method:');

	// If you are going to convert objects to string, you'll use JSON.stringify(variableName).
	console.log(JSON.stringify(batchesArr));
	console.log(typeof JSON.stringify(batchesArr));

	// Using stringify method with variables
		/*
			Syntax:
				JSON.Stringify({
					propertyA: variableA
					propertyB: variableB
				})
		*/


	// User details
	let firstName = prompt('What is your first name?');
	let lastName = prompt('What is your last name?');
	let age = prompt('What is your age?');
	let address = {
		city: prompt('Which city do you live in?'),
		country: prompt('Which country does your city address belongs to?')
	};

	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	});

	console.log(otherData);

	// Converting stringified into JavaScript Objects.
	// Objects are common data types used in applicatations because of the complex data structures taht can be created out of them.

	let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';

	console.log(batchesJSON);

	console.log('Result from parse method:');
	console.log(JSON.parse(batchesJSON));

	let exampleObject = '{"name": "Chris", "age": 16, "isTeenager": false}';

	console.log(JSON.parse(exampleObject));

	// To check data type:
	let exampleParse = JSON.parse(exampleObject);

	console.log(typeof exampleParse.name);
	console.log(typeof exampleParse.age);
	console.log(typeof exampleParse.isTeenager);