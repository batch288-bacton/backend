const express = require('express');
// Mongoose is a package that allows creation of Schemas to Model our data structures.
// Also has access to a number of methods for manipulating our database.
const mongoose = require('mongoose');

const port = 5001;

const app = express();

	// Section: MongoDB Connection.
	// Connect to the database by passing your connection string.
	// Due to update in MongoDB drivers that allow connection, the default connection string is being flagged as an error.
	// by default a warning will be displayed in the terminal when the application is running.
	// {newUrlParser: true}

	// Syntax:
	// mongoose.connect("MongoDB string", {useNewUrlParser:true});

	mongoose.connect("mongodb+srv://admin:admin@batch288bacton.su474nf.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

	// Notification whether we connected properly with the database.

	let db = mongoose.connection;
	
	// for catching the error just in case we had an error during the connection.
	// console.error.bind allows us to print errors in the browser and in the terminal.
	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));

	// if the connection is successful:
	db.once("open", () => console.log("We're connected to the cloud database!"));

	// Middlewares
	// Allows our app to read json data
	app.use(express.json());

	// Allows our app to read data from forms
	app.use(express.urlencoded({extended:true}))

	// [Section] Mongoose Schemas
	// Schemas determine the structure of the document to be written in the database.
	// Schemas act as blueprints to our data.
	// Use the Schema() constructor of the mongoose module to create a new Schema object.


	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type.
		name: String,
		// let us add another field which is status
		status: {
			type: String,
			default: "pending"
		}
	})

	// [Section] Models
	// Uses schema and are used to create/instantiate objects that corresponse to the schema.
	// Models use Schema and they act as the middleman from the server(JS code) to our database.

	// To create a model we are going to use the model();

	const Task = mongoose.model('Task', taskSchema);

	// [Section] Routes

	// Create a POST route to create a new task
	// Create a new task
	// Business Logic:
		// 1. Add a functionality to check whether there are duplicate tasks
			//if the task is existing in the database, we return an error.
			//if the task doesnt exist in the database, we add it in the database
		// 2. The task data will be comming from the request's body.

	app.post("/tasks", (request, response) => {
		// "findOne" method is a mongoose method taht acts similar to "find" in MongoDB.
		// if the findOne finds a document that matches the criteria it will return the object/documents and if there's no object that matches the criteria it will return an empty object or null.
		Task.findOne({name: request.body.name})
		.then(result => {
			// We can use if statement to check or verify whether we have an object found.
			if(result !== null){
				return response.send("Duplicate task found!")
			}else{
				// Create a new task and save it to the database.
				let newTask = new Task({
					name: request.body.name
				})
				// to save the newtask to the collection.
				// The save() method will store the information to the database.
				// since the newTask was created from the Mongoose Schema named Task Model, it will be saved in tasks collection.
				newTask.save()

				return response.send('New task created!')
			}
		})
	})

	// Get all the tasks in our collection
	// 1. Retrieve all the documents
	// 2. if an error is encounter, print the error
	// 3. if no error/s is/are found, send a success statis to the client and show the documents retrieved.

	app.get("/tasks", (request, response) => {
		// fin method is a mongoose method that is similar to MongoDB find.
		Task.find({}).then(result => {
			return response.send(result);
		}).catch(error => response.send(error));
	})

// Activity -------->

	// User Schema
	const userSchema = new mongoose.Schema({
		username: String,
		password: String,
	})

	// Model
	const User = mongoose.model('User', userSchema);

	// Signup Route
	app.post("/signup", (request, response) => {
		User.findOne({username: request.body.username})
		.then(result => {

			if(result !== null){
				return response.send("Duplicate username found!")
			}
			else if(request.body.username === "" || request.body.password === ""){
				return response.send("BOTH username and password must be provided.")
			}
			else{
				let newUser = new User({
					username: request.body.username,
					password: request.body.password
				})

				newUser.save()

				return response.send('New user registered!')
			}
		}).catch(error => response.send(error))
	})











if(require.main === module){
	app.listen(port, () => {
		console.log(`Server is running at port ${port}!`)
	})
}

module.exports = app;