
async function fruitsOnSale(db) {
	return await(


		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$count: "onSale"}
			])

		);
};



async function fruitsInStock(db) {
	return await(

			// Add query here
		db.fruits.aggregate([
			{$match: {stock: {$gte: 20}}},
			{$count: "stock"}
			])

		);
};



async function fruitsAvePrice(db) {
	return await(

			// Add query here
		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {
				supplier: "$supplier_id",
				averagePrice: {$avg: "$price"}
			}}
			])

		);
};



async function fruitsHighPrice(db) {
	return await(

			// Add query here
		db.fruits.aggregate([
			{$group: {
				supplier: "$supplier_id",
				highestPriceOfFruits: {$max: "$price"} 
			}}
			])

		);
};




async function fruitsLowPrice(db) {
	return await(

			// Add query here
		db.fruits.aggregate([
			{$group: {
				supplier: "$supplier_id",
				lowestPriceOfFruits: {$min: "$price"} 
			}}
			])

		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
