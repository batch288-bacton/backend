// [Section] MongoDB aggregation
/*
	- Used to generate manipulated data and perform operations to create filtered results that helps analyzing data.
	- Compare to doing the CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with infromation to make necessary development decisions without having to create a frontend application.
*/

	// Using aggregate method

	db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
		])

	db.fruits.aggregate([
			{$match: {onSale: true}}
		])

	// The $group operator groups the documents in therms of the proterty declared in the _id property
	db.fruits.aggregate([
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
		])

	// Max operator
	db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id",
			 max: {$max: "$stock"}, 
			 sum: {$sum: "$stock"}
			}
		}
		])

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {
			_id: "$color",
			max: {$max: "$stock"},
			sum: {$sum: "$stock"}
		}}
		])

// Field projection with aggrgation
	/*
		- The $project operator can be used when aggregating data to exclude the returned result.
	*/

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {
			_id: "$color",
			max: {$max: "$stock"},
			sum: {$sum: "$stock"}
		}},
		{$project: {_id: 0}}
		])

// Sorting aggregateed result
	/*
		- The $sort operator can be use to change the order of aggregated results.
		- Providing value of -1 will sort the aggregated results in a reverse order.
		- (-)Negative sorts ascending.
		- (+)Positive sorts descending.

		Syntax:
			- {$sort: {field: 1 / -1}}
	*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {
		_id: "$supplier_id",
		total: {$sum: "$stock"}
	}},
	{$sort: {total: 1}}
	])


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {
		_id: "$name",
		stocks: {$sum: "$stock"}
	}},
	{
		$sort: {_id: 1}
	}
	])

// sorting without projecting the id
db.fruits.aggregate([

	{$group: {
		_id: "$color",
		stocks: {$sum: "$stock"}
	}},
	{$project: {_id: 0}},
	{
		$sort: {_id: 1}
	}
	])

// Aggregating results based on an array field
	// unwind operator
	/*
		-The $unwind operator deconstruct an array field from a collection/field with an array value to output a result for each element.

		Syntax:
		{$unwind: field}
	*/

db.fruits.aggregate([
	{$unwind: "$origin"}
	])

// Display fruit documents by their origin and the kinds of fruits that are supplied

// {$sum: 1} - it will count the number of documents in the group
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {
		_id: "$origin",
		kinds: {$sum: 1}
	}},
	{$sort: {kinds: 1, _id:}}
	])

db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {
		_id: "$origin",
		kinds: {$sum: 1}
	}}
	])