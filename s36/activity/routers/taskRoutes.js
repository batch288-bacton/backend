const express = require("express");

const app = express();

const taskControllers = require('../controllers/taskControllers.js');

//Contain all the endpoints of our application
const router = express.Router();

router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks);

// Parameterized

// We are create a route using a Delete method at the URL "/tasks/:id"
//The colon here is an identiefier that helps to create a dynamic route which allows us to supply information
router.delete("/:id", taskControllers.deleteTask);

// GET specific task
router.get("/:id", taskControllers.retrieveTask);

// PUT complete task
router.put("/:id/complete", taskControllers.changeTask);

module.exports = router;