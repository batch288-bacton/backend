const express = require("express");
const mongoose = require("mongoose");

const taskRoutes = require('./routers/taskRoutes.js');


const port = 4001;

const app = express();


// Set up MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch288bacton.su474nf.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {useNewUrlParser:true});


// Check whether we are connected with our db
const db = mongoose.connection;
	
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));

	db.once("open", () => console.log('We are now connected to the db!'));


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//this route/middleware is responsible for the routes in tasks
app.use("/tasks", taskRoutes);




if(require.main === module){
	app.listen(port, () => {
		console.log(`Server is running at port ${port}!`)
	})
}

module.exports = app;